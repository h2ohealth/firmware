#include "RTClib.h"

RTC_DS1307 rtc;

void InitializeRTC() {
  rtc.begin();
  pinMode(13, OUTPUT);
}

unsigned int GetTime() {
  DateTime now = rtc.now();
  Serial.println(now.second(), DEC);
  return rtc.now().unixtime();
}
