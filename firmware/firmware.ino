#include "constants.h"

float temp;
float ppm;


void setup(void) { 
 // start serial port 
 Serial.begin(115200); 
 InitializeTemperature();
 InitializeTDS();
 InitializeSpectrum();
 InitializeRTC();
} 

void loop(void) { 
  temp = GetTemperature();
  TDS test = GetTDS(temp);
  String spectrum = GetSpectrum();
  String time = String(GetTime());
  Serial.println(time);
  SaveString(time + String(temp));
  delay(100);
} 
