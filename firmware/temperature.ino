/********************************************************************/
// First we include the libraries
#include <OneWire.h> 
#include <DallasTemperature.h>
/********************************************************************/
// Setup a oneWire instance to communicate with any OneWire devices  
// (not just Maxim/Dallas temperature ICs) 
OneWire onewire_temp(TEMPERATURE_PORT); 
/********************************************************************/
// Pass our oneWire reference to Dallas Temperature. 
DallasTemperature sensors(&onewire_temp);
/********************************************************************/ 

void InitializeTemperature(void) {
  sensors.begin();
}

float GetTemperature(void) {

  sensors.requestTemperatures();
  float temperature = sensors.getTempCByIndex(0); 
  return temperature;
}
